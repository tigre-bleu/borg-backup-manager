# Warning
I now use Bormatic wich I recommend: https://torsion.org/borgmatic/

This project is not maintained.

# Presentation
borg-backup manager performs in a single command:
- borg create
- borg prune
- borg check
- send an email with the backup status

If borg is used for remote backup over ssh. Passwordless connection needs to be enabled.

Script returns 0 in case of success, 1 in case of failure.

# Install
- git clone https://framagit.org/tigre-bleu/borg-backup-manager.git
- cd borg-backup-manager
- ./install.sh

Install script just copies the borg-backup-manager script to /usr/bin so it is found in the PATH.

# Uninstall
- go to the git local folder
- ./uninstall.sh

Uninstall script just removes the borg-backup-manager script from /usr/bin.

# Usage
Usage from command-line: borg-backup-manager -a archive -r repository -e email -m keep-monthly -y keep-yearly -c files
-a: Archive name
-r: Repository address (borg format)
-e: Email to notify in case of success/failure
-m: How many archives to keep per month
-y: How many archives to keep per year
-c: List of all files to backup. If several folders, include the list between quotes.

Example: `borg-backup-manager -e user@domain -r "backupuser@mydomain:host1" -a data -m 1 -y 1 -c "/var/www/* /var/backups/mysql/*"`

# Usage with Cron
Just add the command line to the crontab

# Usage with Systemd
Folder systemd-examples provides example files for automation with systemd.

## Single backup
- Copy borg-backup-manager.service to /etc/systemd/system/
- Update borg-backup-manager.service with the correct parameters (ExecStartPre/ExecStartPost makes it possible to perform tasks required prior to or after the actual backup)
- Run systemctl enable borg-backup-manager.service
- Run systemctl start borg-backup-manager to start the backup

## Automatic run 'Cron like'
- Copy borg-backup-manager.service to /etc/systemd/system/
- Copy borg-backup-manager.timer to /etc/systemd/system/
- Update borg-backup-manager.service with the correct parameters (ExecStartPre makes it possible to perform tasks required prior to the actual backup)
- Update borg-backup-manager.timer with the correct parameters (See systemd documentation to configure the trigger)
- Run systemctl enable borg-backup-manager.timer
- Run systemctl start borg-backup-manager.timer to arm the trigger
